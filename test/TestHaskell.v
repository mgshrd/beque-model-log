Require Beque.Util.common_extract.
Require Beque.Util.DelayExtractionDefinitionToken.

Require Import Beque.IO.impl.AxiomaticIOInAUnitWorld.
Require Import Beque.IO.intf.IOAux.

Require Import LogImpl.

Extraction Language Haskell.

Module comext.
Include common_extract.commonExtractHaskell DelayExtractionDefinitionToken.token.
End comext.

Module ioext.
Include AxiomaticIOInAUnitWorld.extractHaskell DelayExtractionDefinitionToken.token.
End ioext.

Module ior := IOAux UnitWorld AxiomaticIOInAUnitWorld.
Import ior.

Module log.
Include LogImpl AxiomaticIOInAUnitWorld.UnitWorld AxiomaticIOInAUnitWorld.
End log.
Module loge := log.extractHaskell DelayExtractionDefinitionToken.token.

Program Definition main :=
  _ <== log.log log_level.log_level_info "Hello";;
    _ <-- log.log log_level.log_level_info "World!";;
    /| IO_return tt.

Extraction "test" main.
