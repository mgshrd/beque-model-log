Inductive log_level :=
| log_level_trace
| log_level_debug
| log_level_info
| log_level_notice
| log_level_warning
| log_level_error
| log_level_critical
| log_level_alert
| log_level_emergency
| log_level_fatal.

Definition eq_log_level_dec
           (l1 l2 : log_level) :
  {l1=l2}+{l1<>l2}.
Proof.
  decide equality.
Defined.
