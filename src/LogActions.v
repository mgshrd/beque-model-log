Require Import dec_utils.

Require Import result.
Require Import log_level.
Require Import String.

Module LogActions
.

Inductive log_action :=
| log_action__log : log_level -> string -> log_action.

Definition eq_log_action_dec :
  forall a1 a2 : log_action,
    {a1 = a2}+{a1 <> a2}.
Proof.
  set (H_eq_log_level_dec := eq_log_level_dec).
  set (H_eq_string_dec := string_dec).
  decide equality.
Defined.

Definition log_action_res
           (a : log_action) :
  Set :=
  match a with
  | log_action__log level msg => result unit string
  end.

Definition eq_log_action_res_dec :
  forall (a : log_action) (res1 res2 : log_action_res a),
    {res1 = res2}+{res1 <> res2}.
Proof.
  intros.
  unfold log_action_res in *.
  assert (H_eq_string_dec := string_dec).
  assert (H_eq_unit_dec := eq_unit_dec).
  assert (H_eq_result_dec := eq_result_dec H_eq_unit_dec H_eq_string_dec).
  destruct a; simpl in *; decide equality.
Defined.

End LogActions.
